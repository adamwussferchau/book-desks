const express = require("express");
const bodyParser = require("body-parser");
const Handlebars = require("express-handlebars");
const mongodb = require("mongodb");
const app = express();
const { FormType } = require("./types/index");
require("dotenv").config();

const port = 3000;
const hbs = Handlebars.create({
  helpers: {
    eq: function (l, r) {
      return l === r;
    },
  },
});
app.engine("handlebars", hbs.engine);
app.set("view engine", "handlebars");

const MongoClient = mongodb.MongoClient;
const url = process.env.MONGO_DB_HOST;

app.use(bodyParser.urlencoded({ extended: true }));
app.use(express.static("views"));

async function connectDB() {
  const client = await MongoClient.connect(url, { useUnifiedTopology: true });
  return client
    .db(process.env.MONGO_DB_BASE)
    .collection(process.env.MONGO_DB_TABLE);
}

let desksCollection;

app.use(async (req, res, next) => {
  if (!desksCollection) {
    desksCollection = await connectDB();
  }
  next();
});

app.get("/", async (req, res) => {
  const data = await desksCollection.find({}).toArray();
  const currentDate = new Date();
  for (let i = data.length - 1; i >= 0; i--) {
    if (data[i].isReserved && new Date(data[i].reservation.end) < currentDate) {
      desksCollection.updateOne(
        { id: Number(data[i].id) },
        {
          $set: {
            reservation: null,
            isReserved: false,
          },
        }
      );
      data[i].isReserved = false;
      data[i].reservation = null;
    }
  }

  res.render("main", { layout: "index", data: { tableset: data } });
});

app.get("/add-reservation", async (req, res) => {
  if (!req.query.deskID) {
    res.redirect("/");
  }
  const data = await desksCollection.findOne({ id: Number(req.query.deskID) });
  if (!data) {
    res.redirect("/");
  } else {
    const today = new Date();
    const minDate = today.toISOString().split("T")[0];
    console.log(minDate);
    res.render("tableForm", {
      layout: "index",
      data: {
        desk: data,
        minDate,
        context: {
          button: "Add",
          path: "/add-reservation",
          type: FormType.Add,
        },
      },
    });
  }
});

app.get("/edit-reservation", async (req, res) => {
  if (!req.query.deskID) {
    res.redirect("/");
  }
  const data = await desksCollection.findOne({ id: Number(req.query.deskID) });
  if (!data) {
    res.redirect("/");
  } else {
    const today = new Date();
    const minDate = today.toISOString().split("T")[0];
    res.render("tableForm", {
      layout: "index",
      data: {
        desk: data,
        minDate,
        context: {
          button: "Edit",
          path: "/add-reservation",
          type: FormType.Edit,
        },
      },
    });
  }
});

app.post("/add-reservation", async (req, res) => {
  if (req.body.id && req.body.Person && req.body.from && req.body.to) {
    await desksCollection.updateOne(
      { id: Number(req.body.id) },
      {
        $set: {
          reservation: {
            name: req.body.Person,
            start: req.body.from,
            end: req.body.to,
          },
          isReserved: true,
        },
      }
    );
  }
  res.redirect("/");
});

app.post("/delete-reservation", async (req, res) => {
  if (!req.query.deskID) {
    res.redirect("/");
  } else {
    await desksCollection.updateOne(
      { id: Number(req.query.deskID) },
      {
        $set: {
          reservation: null,
          isReserved: false,
        },
      }
    );
  }
  res.redirect("/");
});

app.listen(port, () => {
  console.log(`Example app listening at http://localhost:${port}`);
});
